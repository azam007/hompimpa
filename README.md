# Hompimpav4
![alt text](http://oi68.tinypic.com/i4rfas.jpg)
## Cara Install
### Important!! Wajib pake Apache Server
* Download atau Clone folder hompimpav4
* Letakkan kedalam folder host biasanya di ```var/www``` atau ```xampp/htdocs```
* Buat Database hompimpa dan import file .sql dengan phpmyadmin ke database tersebut
* Edit file Config yang berada di ``App/Core/Config.php``
* Edit constant ```PATH_GLOBALS``` pada file ```App/init.php``` dengan lokasi folder hompimpa yang baru

### Important!!! Wajib aktivasi mod_rewrite apache
* Tutorial aktifasi nya [Click Here](https://stackoverflow.com/questions/869092/how-to-enable-mod-rewrite-for-apache-2-2)
