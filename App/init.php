	<?php


spl_autoload_register(function($className){
	$className = ltrim($className,'\\');
	$position = strrpos($className, '\\');
	$namespace = substr($className, 0, $position);
	$className = substr($className, $position+1);
	$fileName = str_replace('\\', '/', $namespace).'/'.$className.'.php';
	require_once '../'.$fileName;
});


/*-----------------------------------------------------*\
| kalo built in server pake '/'					        |
| kalo bukan nama folder awal dan akhir pake '/'        |
| contoh :	                                            |
|     const PATH_GLOBALS = '/folder/subfolder/hompimpa/'|
|                        							    |
\*-----------------------------------------------------*/

const PATH_GLOBALS = '/';

$route = new \App\Core\Route();  

// return $route;